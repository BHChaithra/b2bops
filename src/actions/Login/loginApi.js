import { TYPES } from '../../constants/types';
import axios from 'axios';
import { API } from '../../constants/api';


export const loginRequest = () => {
    return {
        type: TYPES.login_request
    }
}

export const loginSuccess = (userDetails) => {
    return {
        type: TYPES.login_success,
        userDetails
    }
}

export const loginFailure = (error) => {
    return {
        type: TYPES.login_failure,
        error
    }
}

export  const loginCheck = (userData, history) => {
    return (dispatch) => {
        dispatch(loginRequest());
        axios.post(API.base_url + API.login, userData).then(
            (response) => {
                console.log(response);
                dispatch(loginSuccess(response.data));
                localStorage.setItem("user",response.data.token);
                // history.push("/home");

            }
        ).catch(
            (error) => {
                console.log(error);
                if(error.message){
                    dispatch(loginFailure(error.message));
                }else{
                    dispatch(loginFailure("Login Error. Try after Sometime"));
                }
            }
        )
    }
}