import React, { Component } from 'react';
import './login.css';
import logo from '../../images/Header/Docket_Logo.svg';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {loginCheck} from '../../actions/Login/loginApi';
import GIF from '../../images/Animated-logo.gif';
import { toast } from 'react-toastify';


class LoginPage extends Component{

    constructor(props){
        super(props);
        this.state = {
            email_id : '',
            password : ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount(){
        if(localStorage.getItem("user")){
            this.props.history.push("/home");
        }
    }

    handleChange(e){
        const name = e.target.name;
        const value = e.target.value;

        this.setState({
            ...this.state, [name] : value
        });
    }

    handleSubmit(){
       console.log("CLic")
        if(this.state.email_id === '' ){
       console.log("CLic" + 1)            
            toast.error("Please enter the mail id", {
                position: toast.POSITION.TOP_RIGHT
              });
        }
        else if(this.state.password === ''){
       console.log("CLic" + 2)            
            toast.error("Please enter the password", {
                position: toast.POSITION.TOP_RIGHT
              });
        }
        else{
            this.props.loginCheck(this.state , this.props.history);
        }
    }


    render(){
        const request = this.props.loginReducer.request;
        return(
            <div className="login-background">
                <section className="login-fields-block">
                    
                    <div className="padding-30">
                        <div className="logo-area">
                            <img src={logo} alt="logo"/>
                        </div>
                        <div className="form-group">
                            <div className="col-md-12 my-email">
                                {/* { this.props.loginReducer.error &&  <p style={{ color : "red" }}>{this.props.loginReducer.error}</p>} */}
                                <label className="has-float-label">
                                    <input id="email" type="email" name="email_id" 
                                        placeholder="example@gmail.com" onChange={this.handleChange} value={this.state.email_id} />
                                    <span>User Id</span>
                                </label>
                            </div>
                        </div>
                       
                        <div className="form-group ">
                            <div className="col-md-12 my-password">
                                <label className="has-float-label">
                                    <input id="password" type="password" name="password"
                                        placeholder="password" onChange={this.handleChange} value={this.state.password} />
                                    <span>Password</span>
                                </label>
                            </div>
                        </div>

                        {
                            request === false &&
                            <img src={GIF} height="100" width="100" />
                        }

                        <br/>

                       <button className="btn login-button" onClick={this.handleSubmit}> Login</button>
                        </div>
                    
                </section>
                
                </div>
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

export default withRouter(connect(mapStateToProps, { loginCheck } )(LoginPage));