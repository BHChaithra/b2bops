import React, { Component } from 'react';
import './Home.css';
import logo from '../../images/Header/Docket_Logo.svg';
import { connect } from 'react-redux';
import { withRouter ,Link} from 'react-router-dom';
import {loginCheck} from '../../actions/Login/loginApi';
import GIF from '../../images/Animated-logo.gif';
import { toast } from 'react-toastify';
import navImg1 from '../../images/Menu/Mousover/Notification_Icon.svg';
import navImg2 from '../../images/Menu/Actual_View/Orders_Icon.svg';
import navImg3 from '../../images/Menu/Actual_View/Make_Payments_Icon.svg';
import navImg4 from '../../images/Menu/Actual_View/Clients_Icon .svg';
import navImg5 from '../../images/Menu/Actual_View/Order_Now_Icon.svg';


class Home extends Component{

    constructor(props){
        super(props);
        this.state = {
           
        }

       
    }

    componentWillMount(){
       
    }

    render(){
        
        return(
           
            <div className="row homeBlock">
                <div className="col-md-12">
                    <div className="col-md-2 navBar">
                    <div id="div1" className="menu-area visible-lg visible-sm visible-md row">
                    <div className="logo-area">
                        <img alt="dummy_image" src={logo} />
                    </div>
                    <div className="menus">
                        <ul>
                                                
                            <li>
                            <div className="description">Notifications</div><img alt="dummy_image" src={navImg1} className="menu-img-1" /><div className="description-line"></div>
                            </li>
                           
                       
                            <Link to="/home/orders">
                            <li>
                            <div className="description">Orders</div><img alt="dummy_image" src={navImg2} className="menu-img-2" /> <div className="description-line"></div>
                            </li>
                            </Link>
                           
                            <li>
                            <div className="description">Payments</div><img alt="dummy_image" src={navImg3} className="menu-img-3" /> <div className="description-line"></div>
                            </li>
                        
                            <li>
                              <div className=" description">Clients</div>  <img alt="dummy_image" src={navImg4} className="menu-img-4" /> <div className="description-line"></div>
                            </li>

                             <Link to="/home" >                            
                            <li>
                            <div className="description">Order Now</div> <img alt="dummy_image" src={navImg5} className="menu-img-5" /><div className="description-line"></div>
                            </li>
                            </Link>
                        </ul>
                    </div>
                    </div>

                    </div>

                    <div className="col-md-10">
                    </div>
                </div>

               
            </div>
           
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

export default Home;