import React, { Component } from 'react';
// import './App.css';
import LoginPage from '../components/Login/LoginPage';
import Home from '../components/Home/Home';
import { BrowserRouter as Router, Route } from "react-router-dom"
import { PrivateRoute } from '../helpers/PrivateRoute';
import { ToastContainer } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';


class App extends Component {
	render() {
		return (
			<div>
			<Router>
				<div>
					<Route exact path="/" component={LoginPage} />
					<PrivateRoute path="/home" component={Home} />
				</div>
			</Router>
			<ToastContainer />
			</div>
		);
	}
}

export default App;
