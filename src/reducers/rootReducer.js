import { combineReducers } from 'redux';
import { loginReducer } from './Login/loginReducer';


const rootReducer = combineReducers({
    loginReducer,
 
}
)

export default rootReducer;