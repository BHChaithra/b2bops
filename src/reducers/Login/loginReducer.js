import { TYPES } from '../../constants/types';

export const loginReducer = (state = {
    request : "",
    success : "",
    error : ""
}, action) => {
    switch (action.type) {
        case TYPES.login_request:
            return Object.assign({}, state,{ request : false });
        case TYPES.login_success:
            return Object.assign({}, state,{ success : action.userDetails , request : true });
        case TYPES.login_failure:
            return Object.assign({}, state,{ error :  action.error});
        default:
            return state;
    }
}